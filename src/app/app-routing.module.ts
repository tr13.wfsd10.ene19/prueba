import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { BuscadorComponent } from './buscador/buscador.component';
import { ResultadosComponent } from './resultados/resultados.component';


const routes: Routes = [
 {path:'header', component:HeaderComponent},
 {path:'buscador',component:BuscadorComponent},
 {path:'footer',component:FooterComponent},
 {path:'resultados/:recipe', component:ResultadosComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
