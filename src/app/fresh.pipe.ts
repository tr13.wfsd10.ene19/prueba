import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'fresh'
})
export class FreshPipe implements PipeTransform {

  transform(values: any[], args?: any): any {
    return values.filter((item )=> item.isFresh);
  }

}
