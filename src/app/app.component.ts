import { Component } from '@angular/core';
import { ServicioService } from 'src/app/service/servicio.service';
import { Subject } from 'rxjs/Subject';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ServicioService]
})
export class AppComponent {
  title = 'prueba';

  results: Object;
  searchTerm$ = new Subject<string>();

  constructor(private _servicioService: ServicioService) {
    this._servicioService.search(this.searchTerm$)
      .subscribe(results => {
        this.results = results['results'];
      });
  }


}
