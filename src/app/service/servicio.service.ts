import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
@Injectable({
  providedIn: 'root'
})
export class ServicioService {
  recipe = [];
  urlRecipepuppy= '/api';
  queryUrl: string = '?search=';
  constructor(private http:HttpClient) { }
  
  getRecipe(recipe:string){
    const recipeUrl = `${this.urlRecipepuppy}?i=${recipe}`;
    
    return this.http.get<any[]>(recipeUrl);

  }
  search(terms: Observable<string>) {
    return terms.debounceTime(400)
      .distinctUntilChanged()
      .switchMap(term => this.searchEntries(term));
  }

  searchEntries(term) {
    return this.http
        .get(this.urlRecipepuppy + this.queryUrl + term)
        .map((res: Response) => res.json());
  }

}
