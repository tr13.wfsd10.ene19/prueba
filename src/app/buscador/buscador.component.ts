import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServicioService } from 'src/app/service/servicio.service';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-buscador',
  templateUrl: './buscador.component.html',
  styleUrls: ['./buscador.component.css'],
 
})
export class BuscadorComponent implements OnInit {
  
 

  constructor(private router: Router, private _servicioService: ServicioService) {
  
   }
  isVisible:boolean=false;
  ngOnInit() {

  }

  verResultados(recipe: String) {
    console.log(recipe);
    this.router.navigate( ['/resultados',recipe] );
  }

  


}
 
