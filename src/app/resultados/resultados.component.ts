import { Component, OnInit } from '@angular/core';
import { ServicioService } from 'src/app/service/servicio.service';
import { ActivatedRoute } from '@angular/router';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-resultados',
  templateUrl: './resultados.component.html',
  styleUrls: ['./resultados.component.css']
})
export class ResultadosComponent implements OnInit {
  resultados = new FormControl('');
  recipes:any[];
  notFound:Boolean = false;
  
  constructor(private _servicioServices:ServicioService, private _activatedRoute:ActivatedRoute, ) {
    
   }

  ngOnInit() {
    this._activatedRoute.params.subscribe( params => {
      this._servicioServices.getRecipe(params['recipe']).subscribe(data => {
        if(data['results'].length >= 1){ 
          console.log(data['results']);
          this.recipes = data['results'];
          this.notFound = false;
        } else {
          this.recipes = [];
          this.notFound = true;
        }
      })
    })
  }


  


}


