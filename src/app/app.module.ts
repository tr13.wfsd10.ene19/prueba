import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { BuscadorComponent } from './buscador/buscador.component';
import { ResultadosComponent } from './resultados/resultados.component';
import { HttpClientModule } from '@angular/common/http';
import { FreshPipe } from './fresh.pipe';
import { ReactiveFormsModule } from '@angular/forms';
import { ServicioService } from './service/servicio.service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    BuscadorComponent,
    ResultadosComponent,
    FreshPipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
 
  ],
  providers: [ServicioService],
  bootstrap: [AppComponent],
 
})
export class AppModule { }
